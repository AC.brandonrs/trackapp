module.exports = (req, res, next) => {
  if (req.user.admin === "false") {
    return res.status(403).send({ error: "Not an Admin" });
  }

  next();
};