const mongoose = require("mongoose");
const requireLogin = require("../middlewares/requireLogin");
const requireAdmin = require("../middlewares/requiredAdmin");
const Rutine = mongoose.model("rutine");

module.exports = app => {

  //obtains every rutine 
  app.get('/api/rutines', requireLogin, async (req, res) => {
    const rutines = await Rutine.find({ creator: req.user.id });
    res.send(rutines);
  })

  //gets a list of rutines based on user name
  app.get("/api/rutines/:userName", requireLogin, async (req, res) => {
    const userRutine = await Rutine.find({ userRutine: req.params.userName }).select(["userRutine", "rutineTitle"]);
    res.send(userRutine);
  });

  //gets a rutine based on the id
  app.get("/api/rutine/:rutineId", requireLogin, async (req, res) => {
    const rutine = await Rutine.find({ _id: req.params.rutineId });
    res.send(rutine);
  });

  //creates a rutine
  app.post("/api/rutines", requireLogin, requireAdmin, async (req, res) => {
    const { rutineUser, rutineTitle, rutineWeek } = req.body;
    //creates rutine with format
    const rutine = new Rutine({
      userRutine: rutineUser,
      creator: req.user._id,
      rutineTitle,
      rutineWeek
    }).save();
    //returns the rutine 
    res.send(rutine);

    /*  const mailer = new Mailer(survey, surveyTemplate(survey));
     try {
       await mailer.send();
       await survey.save();
       req.user.credits -= 1;
       const user = await req.user.save();
       res.send(user);
     } catch (error) {
       res.status(422).send(error);
     } */
  });
};