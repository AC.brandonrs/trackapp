const _ = require("lodash");
const mongoose = require("mongoose");
const requireAdmin = require("../middlewares/requiredAdmin");
const requireLogin = require("../middlewares/requireLogin");


const Clients = mongoose.model("users");

module.exports = app => {

  //obtains all non admin users
  app.get('/api/clients', requireLogin, requireAdmin, async (req, res) => {
    const clients = await Clients.find({ admin: false }).select('userName');
    //clients = _.orderBy(clients,'userName','asc')
    res.send(clients);
  })

};