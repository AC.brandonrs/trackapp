const mongoose = require("mongoose");
const { Schema } = mongoose;
const rutineDaySchema = require("./RutineDay");

const rutineSchema = new Schema({
  userRutine: String,
  creator: {type: Schema.Types.ObjectId, ref: 'User'},
  rutineTitle: String,
  rutineWeek: [rutineDaySchema]
});

mongoose.model("rutine", rutineSchema);