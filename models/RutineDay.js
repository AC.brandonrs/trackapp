const mongoose = require("mongoose");
const { Schema } = mongoose;
const rutineSectionSchema = require("./RutineSection");

const rutineDaySchema = new Schema({
  isRestDay: { type: Boolean, default: false},
  sections: [rutineSectionSchema]
});

module.exports = rutineDaySchema;