const mongoose = require("mongoose");
const { Schema } = mongoose;

const exerciseSchema = new Schema({
  name: String,
  weight: String,
  tool: String,
  reps: String,
  videoHelp: String
});

module.exports = exerciseSchema;