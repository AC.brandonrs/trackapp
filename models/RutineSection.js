const mongoose = require("mongoose");
const { Schema } = mongoose;
const exerciseSchema = require("./Exercise");

const rutineSectionSchema = new Schema({
  type: String,
  time: String,
  exercises: [exerciseSchema]
});

module.exports = rutineSectionSchema;