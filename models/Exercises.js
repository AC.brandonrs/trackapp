const mongoose = require("mongoose");
const { Schema } = mongoose;

const exercisesSchema = new Schema({
  exerciseName: { type: String, default: '', required: true, unique: true },
  videoDemo: { type: String, default: '', required: true },

});

mongoose.model("exercises", exercisesSchema);