import axios from "axios";
import {
  FETCH_USER_FAIL,
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_RUTINES_SUCCESS,
  FETCH_RUTINES_REQUEST,
  FETCH_RUTINES_FAIL,
  FETCH_RUTINE_SUCCESS,
  FETCH_RUTINE_REQUEST,
  FETCH_RUTINE_FAIL,
  FETCH_SURVEYS,
  FETCH_CLIENTS_SUCCESS,
  FETCH_CLIENTS_REQUEST,
  FETCH_CLIENTS_FAIL,
  POST_RUTINE_REQUEST,
  POST_RUTINE_SUCCESS,
  POST_RUTINE_FAIL,
  FETCH_EXERCISES_REQUEST,
  FETCH_EXERCISES_FAIL,
  FETCH_EXERCISES_SUCCESS,
  POST_EXERCISES_REQUEST,
  POST_EXERCISES_FAIL,
  POST_EXERCISES_SUCCESS,
  DELETE_EXERCISES_FAIL,
  DELETE_EXERCISES_SUCCESS,
  DELETE_EXERCISES_REQUEST,
} from "./types";

// gets the current user
export const fetchUser = () => async dispatch => {
  dispatch({ type: FETCH_USER_REQUEST })
  try {
    const res = await axios.get("/api/current_user");
    dispatch({ type: FETCH_USER_SUCCESS, payload: res });
  } catch (error) {
    dispatch({ type: FETCH_USER_FAIL, error });
  }

};
// billing
export const handleToken = (token) => async dispatch => {
  const res = await axios.post('/api/stripe', token);

  dispatch({ type: FETCH_USER_SUCCESS, payload: res });
};
//submit survey
export const submitSurvey = (values, history) => async dispatch => {
  const res = await axios.post('/api/surveys', values);

  history.push('/surveys');
  dispatch({ type: FETCH_USER_SUCCESS, payload: res });
};
//gets surveys
export const fetchSurveys = () => async dispatch => {
  const res = await axios.get('/api/surveys');

  dispatch({
    type: FETCH_SURVEYS,
    payload: res.data
  });
}

//submit rutine
export const submitRutine = (values, history) => async dispatch => {
  dispatch({ type: POST_RUTINE_REQUEST })
  try {
    const res = await axios.post('/api/rutines', values);
    history.push('/rutines');
    dispatch({ type: POST_RUTINE_SUCCESS, payload: res })

  } catch (error) {
    dispatch({ type: POST_RUTINE_FAIL, error })
  }
};

// gets rutines by user
export const fetchRutines = (user) => async dispatch => {
  dispatch({ type: FETCH_RUTINES_REQUEST })
  try {
    const res = await axios.get(`/api/rutines/${user}`);
    dispatch({ type: FETCH_RUTINES_SUCCESS, payload: res.data });
  } catch (error) {
    dispatch({ type: FETCH_RUTINES_FAIL })
  }

}
//get single rutine by id
export const fetchRutine = (rutineId) => async dispatch => {
  dispatch({ type: FETCH_RUTINE_REQUEST })
  try {
    const res = await axios.get(`/api/rutine/${rutineId}`);
    dispatch({
      type: FETCH_RUTINE_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({ type: FETCH_RUTINE_FAIL })
  }
}
//gets clients (users)
export const fetchClients = () => async dispatch => {
  dispatch({ type: FETCH_CLIENTS_REQUEST })
  try {
    const res = await axios.get('/api/clients');
    dispatch({
      type: FETCH_CLIENTS_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({ type: FETCH_CLIENTS_FAIL })
  }

}

export const fetchExercises = () => async dispatch => {
  dispatch({ type: FETCH_EXERCISES_REQUEST })
  try {
    const res = await axios.get('/api/exercises');
    dispatch({
      type: FETCH_EXERCISES_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({ type: FETCH_EXERCISES_FAIL })
  }
}

export const submitExercise = (values) => async dispatch => {
  dispatch({ type: POST_EXERCISES_REQUEST })
  try {
    const post = await axios.post('/api/exercises', values);
    const res = {
      data: {
        success: post.data.success,
        exercise: post.data.exercise,
      }
    }
    dispatch({ type: POST_EXERCISES_SUCCESS, payload: res.data })
  } catch (error) {
    dispatch({ type: POST_EXERCISES_FAIL, payload: error })
  }
};

export const deleteExercise = (id) => async dispatch => {
  dispatch({ type: DELETE_EXERCISES_REQUEST })
  try {
    const deleteOne = await axios.delete(`/api/exercise/${id}`);
    if (deleteOne.data.success) {
      const res = {
        data: {
          success: deleteOne.data.success,
          exerciseId: id,
        }
      }

      dispatch({ type: DELETE_EXERCISES_SUCCESS, payload: res.data })
    }

  } catch (error) {
    dispatch({ type: DELETE_EXERCISES_FAIL, payload: error })
  }
}