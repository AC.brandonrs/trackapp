export const FETCH_USER_REQUEST = 'fetch_user_request';
export const FETCH_USER_SUCCESS = 'fetch_user_success';
export const FETCH_USER_FAIL = 'fetch_user_fail';

export const FETCH_SURVEYS = 'fetch_surveys';

export const FETCH_RUTINES_REQUEST = 'fetch_rutines_request';
export const FETCH_RUTINES_SUCCESS = 'fetch_rutines_success';
export const FETCH_RUTINES_FAIL = 'fetch_rutines_fail';

export const FETCH_CLIENTS_REQUEST = 'fetch_clients_request';
export const FETCH_CLIENTS_SUCCESS = 'fetch_clients_success';
export const FETCH_CLIENTS_FAIL = 'fetch_clients_fail';

export const FETCH_RUTINE_REQUEST = 'fetch_rutine_request';
export const FETCH_RUTINE_SUCCESS = 'fetch_rutine_success';
export const FETCH_RUTINE_FAIL = 'fetch_rutine_fail';


export const POST_RUTINE_REQUEST = 'post_rutine_request';
export const POST_RUTINE_SUCCESS = 'post_rutine_success';
export const POST_RUTINE_FAIL = 'post_rutine_fail';


export const FETCH_EXERCISES_REQUEST = 'fetch_exercises_request';
export const FETCH_EXERCISES_SUCCESS = 'fetch_exercises_success';
export const FETCH_EXERCISES_FAIL = 'fetch_exercises_fail';
export const POST_EXERCISES_REQUEST = 'post_exercises_request';
export const POST_EXERCISES_SUCCESS = 'post_exercises_success';
export const POST_EXERCISES_FAIL = 'post_exercises_fail';
export const DELETE_EXERCISES_REQUEST = 'delete_exercise_request';
export const DELETE_EXERCISES_SUCCESS = 'delete_exercise_success';
export const DELETE_EXERCISES_FAIL = 'delete_exercise_fail';