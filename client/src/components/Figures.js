import React from 'react'
import { Link } from 'react-router-dom'

const Figures = ({ icon, text, hlink }) => {

    
    return(
      <div className="col s4 m4">
          <figure className="orange">
              <figcaption >
                  <h2>
                      <i className="material-icons">{ icon }</i>
                  </h2>
                  <p>
                      {text}
                  </p>
                  <Link to={hlink} ></Link>
              </figcaption>
          </figure>
      </div>
    );
}

export default Figures;