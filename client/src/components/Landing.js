import React from 'react'

const Landing = () => {

    return (
        <div className="landing">
            <h1 className="landing-title">
                Sorrow the prog!
            </h1>
            <div className="row">
                <div className="col s12">
                    <h4 >
                        You will receive an updated rutine every week. <br /> Check your daily rutine here.
                    </h4>
                </div>
            </div>
            <div className="row">
                <div className="col s3 ">
                    <img className="landing-image" src={require('../assets/srw-1.jpg')} alt="" />
                </div>
                <div className="col s3">
                    <img className="landing-image" src={require('../assets/srw 2.jpg')} alt="" />
                </div>
                <div className="col s3">
                    <img className="landing-image" src={require('../assets/srw 3.jpg')} alt="" />
                </div>
                <div className="col s3">
                    <img className="landing-image" src={require('../assets/srw 4.jpg')} alt="" />
                </div>
            </div>

        </div>

    );
}

export default Landing;