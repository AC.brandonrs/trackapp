import _ from 'lodash';
import React from "react";
import { Link } from "react-router-dom";
import menuList from "../utils/menuList";

const SideNav = ({ user, loading }) => {

  const links = _.map(menuList, ({ isLink, icon, text, hlink }) => {
    if (isLink) {
      return (<li key={text}><Link to={hlink} className="waves-effect" ><i className="material-icons">{icon}</i>{text}</Link></li>);
    } else {
      //eslint-disable-next-line
      return (<li key={text}><a className="subheader">{text}</a></li>);
    }
  })

  return (
    <div>
      <ul id="slide-out" className="sidenav orange darken-3">
        {loading ? <li key="userPlaceHolder"><a href="/auth/google">Login With Google</a></li> :
          user != null ?
            <li key={"222"}>
              <div className="user-view">
                <a href="#user"><img className="circle" alt="userImg" src={user.data.userPicture} /></a>
                <a href="#name"><span className="white-text">{user.data.userName}</span></a>
              </div>
              <div className="divider"></div>
            </li>
            :
            <li key="userPlaceHolder"><a href="/auth/google">Login With Google</a></li>
        }
        <li key="Home"><Link to="/" ><i className="material-icons">home</i>Home</Link></li>
        <li key="dvdr"><div className="divider"></div></li>
        {links}
      </ul>

    </div>
  );
}



export default SideNav;
