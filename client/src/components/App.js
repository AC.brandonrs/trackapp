import React, { useEffect } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from '../actions';
import '../scss/App.scss';
import M from 'materialize-css/dist/js/materialize.min.js';
import Landing from './Landing';
import Header from './Header';
import Footer from './Footer';
import RutineNew from './Rutines/RutineNew';
import RutineList from './Rutines/RutineList';
import RutineUsers from './Rutines/RutineUsers';
import Rutine from './Rutines/Rutine/Rutine';
import ExercisesPage from './Exercises/ExercisesPage';

const App = () => {
    const dispatch = useDispatch();
    const userData = useSelector(state => state.auth);
    const { user, loading } = userData;


    useEffect(() => {
        dispatch(actions.fetchUser());

        let sidenav = document.querySelector('#slide-out');
        M.Sidenav.init(sidenav, {});
    }, [dispatch])


    return (
        <div >
            <BrowserRouter>
                {loading ? <Header user={user} loading={loading} /> : <Header user={user} loading={loading} />}
                <div className="container">
                    <Route exact path="/" component={Landing} />
                    <Route exact path="/clients" component={RutineUsers} />
                    <Route exact path="/rutines" component={RutineList} />
                    <Route exact path="/rutine" component={Rutine} />
                    <Route exact path="/rutines/new" component={RutineNew} />
                    <Route exact path="/exercises" component={ExercisesPage} />
                    <Route exact path="/exercises/new" component={RutineList} />
                </div>
                <Footer />
            </BrowserRouter>
        </div>
    );
}

export default App;