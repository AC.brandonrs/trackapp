import React from "react";
import { Link } from "react-router-dom";
import Sidenav from './Sidenav';

const Header = ({ user, loading }) => {

  return (

    <div>
      <nav>
        <div className="nav-wrapper orange darken-3">

          <Link
            to="/"
            className=" brand-logo center"
          >
            Logo
          </Link>
          {/*eslint-disable-next-line*/}
          <a href="#" data-target="slide-out" className="sidenav-trigger show-on-large">
            <i className="material-icons">menu
            </i>
          </a>
          <ul className="right">
            {loading ? <li><a href="/auth/google">Login With Google</a> </li> :
              user != null ?
                <li key="1"><a href="/api/logout">Logout</a></li> :
                <li>
                  <a href="/auth/google">Login With Google</a>
                </li>
            }
          </ul>
          <Sidenav user={user} loading={loading} />
        </div>
      </nav>

    </div>
  );
}



export default Header;
