import React from 'react'


const AddExercises = ({ submitHandler, exerciseName, setExerciseName, videoDemo, setvideoDemo }) => {


  return (
    <div className="row">
      <form >
        <div className="input-field col s4 m4 l4">
          <input id="exercise_name" type="text" className="validate" value={exerciseName} required onChange={e => setExerciseName(e.currentTarget.value)} />
          <label htmlFor="exercise_name">Exercise Name</label>
        </div>
        <div className="input-field col s4 m4 l4">
          <input id="video_demo" type="text" className="validate" value={videoDemo} required onChange={e => setvideoDemo(e.currentTarget.value)} />
          <label htmlFor="video_demo">Video Demo</label>
        </div>
        <div className="input-field col s4 m4 l4">
          <button className="btn waves-effect waves-light geen" type="submit" onClick={e => submitHandler(e, exerciseName, videoDemo)}>
            Add exercise
          </button>
        </div>
      </form >
    </div>
  )
}

export default AddExercises
