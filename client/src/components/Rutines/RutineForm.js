/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import { useHistory } from 'react-router-dom';

import '../../scss/RutineForm.scss';

import { Field, FieldArray, reduxForm, formValueSelector } from 'redux-form'
import { Icon, Button, Modal } from 'react-materialize';

import RutinePreview from './rutinePreview';
import validate from '../../utils/rutineValidate';

import { fetchClients, submitRutine, fetchExercises } from '../../actions';

import { Autocomplete } from '@material-ui/lab';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { CircularProgress, FormControlLabel, Checkbox, TextField } from '@material-ui/core';



const toolsList = [{ 'id': 1, 'name': 'kb' }, { 'id': 2, 'name': 'barbell' }, { 'id': 3, 'name': 'db' }];


const renderField = ({ input, label, type, meta: { touched, error } }) => {
  return (
    <>
      <input placeholder={label} {...input} type={type} />

      {touched && error && <div><span className="red-text"> <InfoOutlinedIcon /> {error}</span></div>}
    </>
  )
};

const selectField = ({ input, label, meta: { touched, error }, options, loading }) => {
  console.log(options);
  return (
    <>
      <Autocomplete
        freeSolo
        options={options}
        loading={loading}
        onChange={input.onChange}
        getOptionLabel={(option) => option.userName || option.name || option.exerciseName}
        renderInput={(params) =>
          <TextField
            {...input}
            {...params}
            label={label}
            InputProps={
              {
                ...params.InputProps,
                endAdornment: (
                  <>
                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </>
                )
              }
            }
          />}
      />
      {touched && error && <div><span className="red-text"> <InfoOutlinedIcon /> {error}</span></div>}
    </>

  )
};

const renderCheckbox = ({ input, label, checkSelected, dayNumber, meta: { touched, error } }) => {
  useEffect(() => {
    checkSelected(input.value, dayNumber)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return (
    <div>
      <FormControlLabel
        control={
          <Checkbox
            checked={input.value ? true : false}
            onChange={input.onChange}
          />
        }
        label={label}
      />
    </div>
  )
}

const renderExercises = ({ fields, meta: { error }, exerciseData: { exercises, loading } }) => {

  return (
    <>
      <div className="col s10 m12 mt mb">
        <button className="btn-small light-grey center" type="button" onClick={() => fields.push()}>
          Add Exercise
        </button>
        {error && <div><span className="red-text"> <InfoOutlinedIcon /> {error}</span></div>}
      </div>
      {fields.map((exercise, index) => (
        <div className="row" key={index}>
          <div className=" col s10">
            <div className="row">
              <div className="input-field col s6 m4">
                <Field name={`${exercise}.name`} component={selectField} label={"Name"} options={exercises} loading={loading} />
              </div>
              <div className="input-field col s4 m2 mt-input">
                <Field name={`${exercise}.reps`} type="text" component={renderField} label={"Reps"} />
              </div>
              <div className="input-field col s6 m4">
                <Field name={`${exercise}.tool`} component={selectField} label={"Tool"} options={toolsList} loading={false} />
              </div>
              <div className="input-field col s4 m2 mt-input">
                <Field name={`${exercise}.weight`} type="text" component={renderField} label={"weight"} />
              </div>
            </div>
          </div>
          <div className=" col s2">
            <button type="button right" onClick={() => fields.remove(index)} className="btn-small red right">
              <Icon >delete_forever</Icon>
            </button>
          </div>
        </div>
      ))}

    </>
  )
};

const renderSections = ({ fields, meta: { error, submitFailed }, exerciseData }) => {
  const [sectionSelected, setSectionSelected] = useState(1);

  return (
    <>
      <div className="input-field col s6 m4 center">
        <button className="btn light-grey  mb" type="button" onClick={() => fields.push({})}>
          Add section
        </button>
      </div>
      <div className="input-field col s6 m4">
        {fields.length > 1 ?
          <select id="sectionSelect" className="selectForm grey lighten-4" onChange={e => { setSectionSelected(e.currentTarget.value) }} value={sectionSelected}>
            {
              fields.map((field, index) => (
                <option key={index} value={index + 1}>
                  Section: {index + 1}
                </option>
              ))
            }
          </select> :
          null
        }
        {submitFailed && error && <div><span className="red-text"> <InfoOutlinedIcon /> {error}</span></div>}
      </div>
      {
        fields.map((section, index) => (

          // eslint-disable-next-line eqeqeq
          <div key={index} className="col s12 m10 offset-m1 mt" hidden={sectionSelected != (index + 1)}>
            <button type="button" title="Remove section" onClick={() => { fields.remove(index); setSectionSelected(1) }} className="btn-small red right">
              <Icon >delete_forever</Icon>
            </button>
            <h5>section #{index + 1}</h5>
            <div className="input-field col s6 m4">
              <Field name={`${section}.type`} id={`${section}.type`} type="text" component={renderField} label="Type:" />
            </div>
            <div className="input-field col s6 m4">
              <Field name={`${section}.time`} id={`${section}.time`} type="text" component={renderField} label="Time" />
            </div>

            <FieldArray name={`${section}.exercises`} component={renderExercises} exerciseData={exerciseData} />
          </div>
        ))
      }
    </>
  )
};

const renderDays = ({ fields, meta: { error, submitFailed }, exerciseData }) => {

  const [daySelector, setDaySelector] = useState(1);
  const [checkBoxArray, setCheckBoxArray] = useState([false, false, false, false, false, false, false,]);

  const checkSelected = (value, dayNumber) => {
    const newCheckArray = checkBoxArray.slice()
    newCheckArray[dayNumber] = value
    setCheckBoxArray(newCheckArray);
  }
  return (
    <>
      <div className="input-field col s6 m4 center">
        <button className="btn light-grey  mb" type="button" onClick={() => fields.push({})}>
          Add a day
        </button>
      </div>
      <div className="input-field col s4 m3">
        {
          fields.length > 1 ?
            <select id="daySelect" className="selectForm" onChange={e => { setDaySelector(e.currentTarget.value) }} value={daySelector}>
              {
                fields.map((field, index) => (
                  <option key={index} value={index + 1}>
                    Day: {index + 1}
                  </option>
                ))
              }
            </select> :
            null
        }
        {submitFailed && error && <div><span className="red-text"> <InfoOutlinedIcon /> {error}</span></div>}
      </div>

      {fields.map((day, index) => (
        // eslint-disable-next-line eqeqeq
        <div key={"day" + index} className="col s12 mb dayCard grey lighten-4" hidden={daySelector != (index + 1)} >
          <h5 className="center">
            Day #{index + 1}
          </h5>

          <div className="center">
            <Field name={`${day}.isRestDay`} label="Rest Day" component={renderCheckbox} checkSelected={checkSelected} dayNumber={index} />
          </div>
          {checkBoxArray[index] ?
            <div className="center">
              <h4>Rest Day!!</h4>
            </div>
            :
            <>
              <FieldArray name={`${day}.sections`} component={renderSections} exerciseData={exerciseData} />
              {submitFailed && error && <span className='red-text'>{error}</span>}
            </>
          }

        </div>
      ))}

    </>
  )
};

let RutineForm = ({ pristine, reset, submitting, rutineData, handleSubmit }) => {

  // redux data from selectors
  const clientsData = useSelector(state => state.clients);
  const exerciseData = useSelector(state => state.exercise);
  const dispatch = useDispatch();
  const history = useHistory();

  const { clients, loading } = clientsData;

  useEffect(() => {
    //dispatchs section
    dispatch(fetchClients());
    dispatch(fetchExercises());

  }, [dispatch]);

  const submitHandler = () => {
    dispatch(submitRutine(rutineData, history));
  }

  return (
    <div className="mt-mid mb-mid">
      <div className="row">
        <div className="col s8 offset-s2 mb">
          <h4 className="center">Create a rutine! </h4>
        </div>
        <div className="row">
          <div className="col s12">
            <form onSubmit={handleSubmit(submitHandler)}>
              <div className=" input-field col s8 m8 l8">
                <Field name="rutineTitle" id={"rutineTitle"} type="text" component={renderField} label="Rutine Title" />
              </div>
              <div className="input-field col s8 m6">
                <Field name="rutineUser" label="Select User" component={selectField} options={clients} loading={loading} />
              </div>
              <div className="col s12">
                <FieldArray name="rutineWeek" component={renderDays} exerciseData={exerciseData} />
              </div>
              <div className="col s4 mt-mid">
                <button className="btn light-green right" type="submit" disabled={submitting}>
                  Submit
                </button>
              </div>
              <div className="col s4 mt-mid">
                <button className="btn light-blue center" type="button" disabled={pristine || submitting} onClick={reset}>
                  Clear Values
                </button>
              </div>
              <div className="col s4 mt-mid">
                <Modal
                  actions={[
                    <Button flat modal="close" node="button" waves="green">Close</Button>
                  ]}
                  bottomSheet={false}
                  fixedFooter={false}
                  header="Rutine Preview"
                  id="Modal-0"
                  open={false}
                  options={{
                    dismissible: true,
                    endingTop: '10%',
                    inDuration: 350,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    opacity: 0.5,
                    outDuration: 350,
                    preventScrolling: true,
                    startingTop: '4%'
                  }}
                  trigger={<Button node="button">Preview Rutine</Button>}
                >
                  <RutinePreview {...rutineData} />
                </Modal>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  )
}


const selector = formValueSelector('rutineForm') // <-- same as form name
RutineForm = connect(state => {
  const { rutineUser, rutineTitle, rutineWeek } = selector(state, 'rutineUser', 'rutineTitle', 'rutineWeek');
  return {
    rutineData: {
      rutineUser,
      rutineTitle,
      rutineWeek
    }
  }
})(RutineForm)

export default reduxForm({
  validate,
  form: 'rutineForm'
})(RutineForm);