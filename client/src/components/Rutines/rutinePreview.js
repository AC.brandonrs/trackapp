import React from 'react';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import Days from './Rutine/Days';

const RutinePreview = ({ rutineTitle, rutineWeek }) => {
  return (
    <div className="row">
      <div className="col s12">
        <div className="card-content">
          <h4 className="card-title center-align">
            {rutineTitle  /*change*/}
          </h4>
        </div>
        <div className="card-action">
          <div className="card-content">
            {rutineWeek && rutineWeek !== undefined ?
              <Days rutineDays={rutineWeek} /> :
              <div className="error">
                <div className="row">
                  <div className="col s12">
                    <h4>Waiting for days with rutines</h4>
                  </div>
                </div>
              </div>
            }

          </div>
        </div>
      </div>
    </div>
  )
}

function mapStateToProps(state) {
  return { rutineData: getFormValues('rutineForm')(state) };
}


export default connect(mapStateToProps)(RutinePreview);
