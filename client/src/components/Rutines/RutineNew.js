import React from 'react'
import { reduxForm } from 'redux-form';
import RutineForm from './RutineForm';

const RutineNew = () => {
  return (
    <RutineForm />
  )
}
//change
export default reduxForm({
  form: 'rutineForm'
})(RutineNew);