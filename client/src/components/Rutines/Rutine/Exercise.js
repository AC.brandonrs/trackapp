import React from 'react'
import { Icon } from 'react-materialize';

const Exercise = ({ exercises }) => {


  return (
    <table className="centered responsive-table">
      <thead>
        <tr>
          <th>Exercise</th>
          <th>Reps</th>
          <th>Tool</th>
          <th>weight</th>
          <th>Video Help</th>
        </tr>
      </thead>

      <tbody>
        {exercises !== undefined ?
          exercises.map((exercise, index) => {
            return (
              exercise !== undefined ?
                <tr key={index}>
                  <td>{exercise.name}</td>
                  <td>{exercise.reps}</td>
                  <td>{exercise.tool}</td>
                  <td>{exercise.weight}</td>
                  <td><a target="_blank" rel="noopener noreferrer" href={exercise.videoHelp || null}><Icon>keyboard_capslock</Icon></a></td>
                </tr> : null
            );
          }) : null}
      </tbody>
    </table>

  )
}

export default (Exercise);