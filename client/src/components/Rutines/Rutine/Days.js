import React from 'react'
import { Collapsible, CollapsibleItem, Icon } from 'react-materialize';
import Exercise from './Exercise';

const Days = ({ rutineDays }) => {

  return (
    <Collapsible accordion>
      {rutineDays !== undefined ? 
       rutineDays.map((day, index) => {
          return (
            <CollapsibleItem
              expanded={false}
              header={`Day # ${index + 1}`}
              icon={<Icon>filter_drama</Icon>}
              node="div"
              key={index + 1}>
              { day.isRestDay ?  
                 <div className="">
                   <h4>Rest Day</h4>
                 </div>
                :  day.sections !== undefined ? 
                    day.sections.map((section, index) => {
                      return (
                        <div className="row" key={section._id || index}>
                          <div className="col s6">
                            <h4 className="right-alighn">{section.type ? section.type : "Waiting for a type"}</h4>
                            <h6 className="right-alighn">Time: <span>{section.time ? section.type : "Waiting for a type"}</span> </h6>
                          </div>
                          <div className="col s12 ">
                            {section.exercises !== undefined ? < Exercise exercises={section.exercises} /> :
                              <div className="row">
                                <div className="col s12">
                                  <h5>Waiting for exercises</h5>
                                </div>
                              </div>
                            }
                          </div>
                        </div>
                      )
                    })
                    :
                    <div className="">
                      No Sections to Load
                    </div>
                  
              }
            </CollapsibleItem>
          )
        })
       : 
        <div className="">
          <h5>No days to load</h5>
        </div> 
      }
    </Collapsible>
  )
}



export default (Days);