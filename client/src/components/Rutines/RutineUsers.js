import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { fetchClients } from '../../actions'; //change 
import Clients from './Clients';
const RutineUsers = () => {

  const clientsData = useSelector(state => state.clients);
  const { clients, loading } = clientsData;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchClients());
  }, [dispatch]);


  return (
    <div className="mt">
      <div className="row">
        {loading ?
          <div className="mt-big">
            <div className="progress">
              <div className="indeterminate"></div>
            </div>
          </div>
          :
          clients.map(client => {
            return (
              <Clients key={client._id} client={client} />

            )
          })
        }
      </div>
      <div className="filler-content-2"></div>
    </div>
  )
}



export default RutineUsers;