export default [
  {isLink:false, icon:"assignment",text:"Rutines", hlink:"#"},
  {isLink:true, icon:"accessibility",text:"User rutines", hlink:"/clients"},
  {isLink:true, icon:"create_new_folder",text:"Create rutine", hlink:"/rutines/new"},
  {isLink:false, icon:"more_horiz",text:"Settings", hlink:"#"},
  {isLink:true, icon:"fitness_center", text:"Exercises's list", hlink:"/exercises"},
  {isLink:true, icon:"add_box", text:"Add Exercise to list", hlink:"/exercises/new"}
]