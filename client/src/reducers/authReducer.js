import { FETCH_USER_FAIL, FETCH_USER_REQUEST, FETCH_USER_SUCCESS } from "../actions/types";

export default function (state = { loading: true, user: null }, action) {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return { loading: true, user: null };
    case FETCH_USER_SUCCESS:
      if (action.payload.data === "") {
        return { loading: true, user: null };
      } else {
        return { loading: false, user: action.payload }
      }
    case FETCH_USER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}
