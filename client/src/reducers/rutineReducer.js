import { FETCH_RUTINE_SUCCESS, FETCH_RUTINE_REQUEST, FETCH_RUTINE_FAIL, POST_RUTINE_FAIL, POST_RUTINE_SUCCESS, POST_RUTINE_REQUEST } from '../actions/types';

export default function (state = { loading: true, rutine: {} }, action) {
  switch (action.type) {
    case FETCH_RUTINE_REQUEST:
      return { loading: true };
    case FETCH_RUTINE_SUCCESS:
      return { loading: false, rutine: action.payload };
    case FETCH_RUTINE_FAIL:
      return { loading: false, error: action.payload };
    case POST_RUTINE_REQUEST:
      return { loading: true };
    case POST_RUTINE_SUCCESS:
      return { loading: false, rutine: action.payload };
    case POST_RUTINE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}