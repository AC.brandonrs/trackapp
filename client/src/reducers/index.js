import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form'
import authReducer from './authReducer';
import surveysReducer from './surveysReducer';
import rutinesReducer from './rutinesReducer';
import clientsReducer from './clientReducer';
import rutineReducer from './rutineReducer';
import exerciseReducer from './exerciseReducer';
//  key : reducer 
export default combineReducers({
    auth: authReducer,
    form: reduxForm,
    surveys: surveysReducer,
    rutines: rutinesReducer,
    clients: clientsReducer,
    rutine: rutineReducer,
    exercise: exerciseReducer,
});