import { FETCH_CLIENTS_FAIL, FETCH_CLIENTS_REQUEST, FETCH_CLIENTS_SUCCESS } from "../actions/types";

export default function (state = { clients: [], loading: true }, action) {
  switch (action.type) {
    case FETCH_CLIENTS_REQUEST:
      return { loading: true };
    case FETCH_CLIENTS_SUCCESS:
      return { loading: false, clients: action.payload };
    case FETCH_CLIENTS_FAIL:
      return { loading: false, error: action.payload };;
    default:
      return state;
  }
}
